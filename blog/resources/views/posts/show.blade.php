@extends('layouts.mastercast')

@section('title')
   Detail {{$detail->nama}}
@endsection

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Detail info</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
       <p>Nama : {{ $detail->nama }}</p>
       <p>Umur : {{ $detail->umur }}</p>
       <p>Bio  : {{ $detail->bio }}</p>
    </div>
    
  </div>
@endsection