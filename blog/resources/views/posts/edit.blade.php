@extends('layouts.mastercast')

@section('title')
    Edit cast {{$edit->nama}}
@endsection

@section('content')
<div class="mt-0">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Buat data pemain film baru</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/cast/{{$edit->id}}" method="POST">
          @csrf
          @method('put')
          <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Nama</label>
              <input type="text" value="{{$edit->nama}}" class="form-control" name="nama"  placeholder="Masukkan Nama">

              @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>

            <div class="form-group">
              <label for="exampleInputPassword1">Umur</label>
              <input type="number"  value="{{$edit->umur}}" class="form-control" name="umur" placeholder="Masukkan Umur">
              
              @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>

            <div class="form-group">
              <label for="exampleInputFile">Bio</label>
              <input type="text"  value="{{$edit->bio}}" class="form-control" name="bio" placeholder="Masukkan Password">

              
              @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
          </div>
          
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-success">Update</button>
          </div>
        </form>
      </div>
</div>

@endsection