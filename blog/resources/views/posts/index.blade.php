@extends('layouts.mastercast')

@section('content')

    <div class="mt-2 ml-3">
        <div class="card">
          
            <div class="card-header">
              <h3 class="card-title">Cast Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <a href="cast/create" class="btn btn-success btn-sm mb-3">Tambah</a>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="width: 8px">No</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse($posts as $key => $cast)
                    <tr>
                        <td> {{ $key +1 }}</td>
                        <td> {{ $cast->nama }}</td>
                        <td> {{ $cast->umur }}</td>
                        <td> {{ $cast->bio }}</td>
                        <td> 
                          <form action="/cast/{{$cast->id}}" method="POST">
                            <a href="/cast/{{ $cast->id }}" class="btn btn-primary btn-sm">Detail</a>
                            <a href="/cast/{{ $cast->id }}/edit" class="btn btn-success btn-sm">Edit</a>
                              @csrf
                              @method('delete')
                              <input type="submit" class="btn btn-danger btn-sm" value="delete">
                          </form>
                        </td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="4" allign='center'> No post available</td>
                        </tr>
                  @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
              <ul class="pagination pagination-sm m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">«</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">»</a></li>
              </ul>
            </div> 
          </div>
    </div>
@endsection