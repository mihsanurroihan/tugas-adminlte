<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <!--Header-->
    <h1> SignUp Form</h1>

    <!--Konten-->
    <form action="/welcome" method="post">
        @csrf 
        <label>First Name :</label><br><br>
        <input type="text" name="firstname" placeholder="Masukkan first name"> <br><br>
        <label>Last Name :</label><br><br>
        <input type="text" name="lastname" placeholder="Masukkan last name"><br><br>
        <label>Gender :</label><br>
        <input type="radio" name="gender"> Male<br>
        <input type="radio" name="gender"> Female<br>
        <input type="radio" name="gender"> Other<br><br>

        <label>Nationality</label><br>
        <select name="nation"><br>
            <option value="indonesia"> Indonesia</option>
            <option value="amerika"> Amerika</option>
            <option value="wakanda"> Wakanda</option>
        </select><br><br>

        <label>Language spoken :</label><br>
        <input type="checkbox"> Bahasa Indonesia<br>
        <input type="checkbox"> Bahasa Inggris<br>
        <input type="checkbox"> Lainnya<br><br>

        <label>Bio :</label><br>
        <textarea name="Bio" cols="40" rows="10" placeholder="Masukkan alamat"></textarea><br><br>
        <input type="submit" SignUp> 
    </form>
</body>
</html>