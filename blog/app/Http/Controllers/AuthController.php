<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register.register');
    }

    public function welcome(Request $request)
    {
        //dd($request->all());
        $firstname = $request["firstname"];
        $lastname = $request["lastname"];
        $gender = $request["gender"];
        $nation = $request["nation"];
        $bio = $request["Bio"];

        return view('register.welcome', compact('firstname','lastname','gender','nation','bio'));
    }
}
