<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'nama' => 'required|unique:cast|max:255',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query=DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function index()
    {
        $posts=DB::table('cast')->get();
        // dd($posts);
        return view('posts.index',compact('posts'));
    }

    public function show($id)
    {
        $detail = DB::table('cast')->where('id', $id)->first();
        return view('posts.show', compact('detail'));
    }

    public function edit($id)
    {
        $edit = DB::table('cast')->where('id', $id)->first();
        return view('posts.edit', compact('edit'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|unique:cast|max:255',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        DB::table('cast')
            ->where('id',$id)
            ->update(
                [
                'nama'=> $request['nama'],
                'umur'=> $request['umur'],
                'bio'=> $request['bio'],
                ]
            );
        return redirect('/cast');
    }
    
    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');

    }
}
